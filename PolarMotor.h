/*----- PROTECTED REGION ID(PolarMotor.h) ENABLED START -----*/
//=============================================================================
//
// file :        PolarMotor.h
//
// description : Include for the PolarMotor class.
//
// project :     .
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL:  $
//
// CVS only:
// $Source:  $
// $Log:  $
//
//=============================================================================
//                This file is generated by POGO
//        (Program Obviously used to Generate tango Object)
//=============================================================================


#ifndef POLARMOTOR_H
#define POLARMOTOR_H


#include <tango.h>

#include "Rs232Client.hpp"

/*----- PROTECTED REGION END -----*/


/**
 *	PolarMotor class Description:
 *
 */

namespace PolarMotor_ns
{
	/*----- PROTECTED REGION ID(PolarMotor::Additional Class Declarations) ENABLED START -----*/

		//		Additional Class Declarations

	/*----- PROTECTED REGION END -----*/	//	PolarMotor::Additional Class Declarations


class PolarMotor : public Tango::Device_4Impl
{


	/*----- PROTECTED REGION ID(PolarMotor::Data Members) ENABLED START -----*/

	//		Add your own data members
	IO::Rs232Client *port;
	/*----- PROTECTED REGION END -----*/	//	PolarMotor::Data Members


//	Device property data members
public:		//	Device_port:
	string	device_port;


//	Attribute data members
public:
	Tango::DevULong	*attr_Superstep_read;



//	Constructors and destructors
public:
	/**
	 * Constructs a newly allocated Command object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	PolarMotor(Tango::DeviceClass *cl,string &s);
	/**
	 * Constructs a newly allocated Command object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device Name
	 */
	PolarMotor(Tango::DeviceClass *cl,const char *s);
	/**
	 * Constructs a newly allocated Command object.
	 *
	 *	@param cl	Class.
	 *	@param s 	Device name
	 *	@param d	Device description.
	 */
	PolarMotor(Tango::DeviceClass *cl,const char *s,const char *d);
	/**
	 * The object destructor.
	 */
	~PolarMotor() {delete_device();};



//	Miscellaneous methods
public:
	/**
	 *	will be called at device destruction or at init command.
	 */
	void delete_device();
	/**
	 *	Initialize the device
	 */
	virtual void init_device();
	/**
	 *	Read the device properties from database
	 */
	 void get_device_property();
	/**
	 *	Always executed method before execution command method.
	 */
	virtual void always_executed_hook();


//	Attribute methods
public:
	/**
	 *	Method      : PolarMotor::read_attr_hardware()
	 *	Description : Hardware acquisition for attributes.
	 */
	virtual void read_attr_hardware(vector<long> &attr_list);


	/**
	 *	Superstep attribute related methods.
	 *	Description:
	 *
	 *	Data type:	Tango::DevULong
	 *	Attr type:	Scalar
	 */
	virtual void read_Superstep(Tango::Attribute &attr);
	virtual void write_Superstep(Tango::WAttribute &attr);
	virtual bool is_Superstep_allowed(Tango::AttReqType type);



	/**
	 *	Method      : PolarMotor::add_dynamic_attributes()
	 *	Description : Add dynamic attributes if any.
	 */
		void add_dynamic_attributes();

//	Command related methods
public:


	/**
	 *	Command DoStepCW related methods.
	 */
	void do_step_cw();
	virtual bool is_DoStepCW_allowed(const CORBA::Any &any);

	/**
	 *	Command DoStepCCW related methods.
	 */
	void do_step_ccw();
	virtual bool is_DoStepCCW_allowed(const CORBA::Any &any);

	/**
	 *	Command DoSuperStepCW related methods.
	 */
	void do_super_step_cw();
	virtual bool is_DoSuperStepCW_allowed(const CORBA::Any &any);

	/**
	 *	Command DoSuperStepCCW related methods.
	 */
	void do_super_step_ccw();
	virtual bool is_DoSuperStepCCW_allowed(const CORBA::Any &any);

	/**
	 *	Command On related methods.
	 */
	void on();
	virtual bool is_On_allowed(const CORBA::Any &any);

	/**
	 *	Command Off related methods.
	 */
	void off();
	virtual bool is_Off_allowed(const CORBA::Any &any);



	/*----- PROTECTED REGION ID(PolarMotor::Additional Method prototypes) ENABLED START -----*/

	//	Additional Method prototypes

	/*----- PROTECTED REGION END -----*/	//	PolarMotor::Additional Method prototypes

};

	/*----- PROTECTED REGION ID(PolarMotor::Additional Classes Definitions) ENABLED START -----*/

	//	Additional Classes definitions

	/*----- PROTECTED REGION END -----*/	//	PolarMotor::Additional Classes Definitions

} //	namespace

#endif	//	POLARMOTOR_H
